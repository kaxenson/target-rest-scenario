# myRetail RESTful Service Scenario

## Starting the Application

   This is a Spring Boot application. In order run it:

1. Change directory to where the pom.xml is.
2. To start the application it is easiest to run this Maven command.
    > mvn spring-boot:run
3. To stop use ctrl-C.

## Testing the Application 

   To test the application run the curl commands given below, or access the URLs from a browser.
   

1. Query to get product 1492 for which we expect the name to be "not found" and value be empty, run this on curl command.

     > curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/myretail/product/1492

         HTTP/1.1 200
         Content-Type: application/json;charset=UTF-8
         Transfer-Encoding: chunked
         Date: Tue, 16 May 2017 18:41:25 GMT


         {"id":"1492","name":"not found","value":"","currencyCode":"USD"}

   

2. Query to get product 13860428 for which we expect the name to be "The Big Lebowski (Blu-ray)" and value = 19.99, run this on curl command.

     > curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/myretail/product/13860428

         HTTP/1.1 200
         Content-Type: application/json;charset=UTF-8
         Transfer-Encoding: chunked
         Date: Tue, 16 May 2017 18:43:02 GMT
         {"id":"13860428","name":"The Big Lebowski (Blu-ray)","value":"14.99","currencyCode":"USD"}


   
3. I also added a URL to test if the controller responds to the most simple URL.

     > curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/myretail/product/test

         HTTP/1.1 200
         Content-Type: application/json;charset=UTF-8
         Content-Length: 4
         Date: Tue, 16 May 2017 18:47:42 GMT