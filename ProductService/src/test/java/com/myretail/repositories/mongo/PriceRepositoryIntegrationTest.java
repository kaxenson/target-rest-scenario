package com.myretail.repositories.mongo;

import com.myretail.config.ApplicationConfig;
import com.myretail.domain.mongo.Price;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration // If using this, then you do not need to create a MongoConfig
@ComponentScan(basePackageClasses = {ApplicationConfig.class})//, MongoConfig.class})
@EnableMongoRepositories(basePackageClasses = { PriceRepository.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class PriceRepositoryIntegrationTest {
    @Autowired
    private PriceRepository priceRepository;

    @Before
    public void cleanup() {
        priceRepository.deleteAll();
    }

    @Test
    public void testPriceRepository_MongoDB() {
        assertEquals(0, priceRepository.count());

        // Save one Price
        Price newPrice = new Price("12345678", "1.00");
        Price savedPrice = priceRepository.save(newPrice);
        assertEquals(1, priceRepository.count());

        // Find Price by ID
        Price foundPrice = priceRepository.findOne(savedPrice.getId());
        assertEquals("1.00", foundPrice.getValue());

        // Find Price by ProductId
        foundPrice = priceRepository.findByProductId("12345678");
        assertEquals("1.00", foundPrice.getValue());

        // What if the there are duplicate product ids
        // Mostly for learning about MongoDB
        Price duplicate = new Price("12345678", "42.00");
        priceRepository.save(duplicate);
        assertEquals(2, priceRepository.count());
        foundPrice = priceRepository.findByProductId("12345678");
        assertEquals("1.00", foundPrice.getValue());
        List<Price> prices = priceRepository.findAll();
        assertEquals("12345678", prices.get(0).getProductId());
        assertEquals(prices.get(0).getProductId(), prices.get(1).getProductId());
        assertNotEquals(prices.get(0).getId(), prices.get(1).getId());
        assertNotEquals(prices.get(0).getValue(), prices.get(1).getValue());

    }
}
