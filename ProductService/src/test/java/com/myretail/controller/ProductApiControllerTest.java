package com.myretail.controller;

import com.myretail.config.ApplicationConfig;
import com.myretail.repositories.mongo.PriceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {ApplicationConfig.class})//, MongoConfig.class})
@EnableMongoRepositories(basePackageClasses = { PriceRepository.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ProductApiControllerTest {

    @Test
    public void contextLoads_Placeholder() {
    }
  /*  TODO: finish this if time permits
    @Autowired
    public MockMvc mockMVC;

    @Autowired
    private RestTemplate restTemplate;

    private ProductService productService;
    private PriceRepository mockPriceRepository;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockPriceRepository = Mockito.mock(PriceRepository.class);

        productService = new ProductServiceImpl(mockPriceRepository, restTemplate);
    }

    @Test
    public void testGetProductById() throws Exception{
        mockMVC.perform(get("/product/13860428"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    }*/
}
