package com.myretail.domain;


import com.myretail.domain.mongo.Price;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Iterator;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PriceTest {
    private static Validator validator;

    @BeforeClass
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testProductId_IsNull() {
        Price price = new Price(null, "1.00");

        Set<ConstraintViolation<Price>> constraintViolations =
                validator.validate(price);

        assertEquals(1, constraintViolations.size());
        assertEquals("ProductId may not be null", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void testProductId_Pattern() {
        Price price = new Price("1234567", "1.00");

        Set<ConstraintViolation<Price>> constraintViolations =
                validator.validate(price);

        assertEquals(1, constraintViolations.size());
        assertEquals(
                "ProductId is exactly 8 digits",
                constraintViolations.iterator().next().getMessage()
        );

        price = new Price("123456789", "1.00");

        constraintViolations =
                validator.validate(price);

        assertEquals(1, constraintViolations.size());
        assertEquals(
                "ProductId is exactly 8 digits",
                constraintViolations.iterator().next().getMessage()
        );

        price = new Price("12345678", "1.00");

        constraintViolations = validator.validate(price);
        assertEquals(0, constraintViolations.size());

    }

    @Test
    public void testValue_IsNull() {
        Price price = new Price("1", null);

        Set<ConstraintViolation<Price>> constraintViolations =
                validator.validate(price);

        assertEquals(1, constraintViolations.size());
        assertEquals(
                "ProductId is exactly 8 digits",
                constraintViolations.iterator().next().getMessage()
        );
    }

    @Test
    public void testValue_Pattern() {
        Price price = new Price("12345678", "9.99");

        Set<ConstraintViolation<Price>> constraintViolations =
                validator.validate(price);
        assertEquals(0, constraintViolations.size());

        price = new Price("12345678", "999999.99");
        constraintViolations =
                validator.validate(price);
        assertEquals(1, constraintViolations.size());
        assertEquals(
                "must match \"^\\d{1,5}\\.\\d{2}\"",
                constraintViolations.iterator().next().getMessage()
        );

        price = new Price("12345678", "9.999");
        constraintViolations =
                validator.validate(price);
        assertEquals(1, constraintViolations.size());
        assertEquals(
                "must match \"^\\d{1,5}\\.\\d{2}\"",
                constraintViolations.iterator().next().getMessage()
        );

        price = new Price("12345678", "-9.99");
        constraintViolations =
                validator.validate(price);
        assertEquals(1, constraintViolations.size());
        assertEquals(
                "must match \"^\\d{1,5}\\.\\d{2}\"",
                constraintViolations.iterator().next().getMessage()
        );
    }
}
