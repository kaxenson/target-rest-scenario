package com.myretail.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myretail.config.ApplicationConfig;
import com.myretail.domain.mongo.Price;
import com.myretail.model.ProductSummary;
import com.myretail.repositories.mongo.PriceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(SpringRunner.class)
@EnableAutoConfiguration // If using this, then you do not need to create a MongoConfig
@ComponentScan(basePackageClasses = {ApplicationConfig.class})//, MongoConfig.class})
@EnableMongoRepositories(basePackageClasses = { PriceRepository.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ProductServiceImplTest {

    @Autowired
    private RestTemplate restTemplate;

    private ProductService productService;
    private PriceRepository mockPriceRepository;

    private MockRestServiceServer mockServer;

    @Before
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mockPriceRepository = Mockito.mock(PriceRepository.class);

        productService = new ProductServiceImpl(mockPriceRepository, restTemplate);
    }


    @Test
    public void testGetProductById() {
        String url = "http://redsky.target.com/v2/pdp/tcin/13860428?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics";
        mockServer.expect(requestTo(url))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withSuccess(sampleResponse(), MediaType.APPLICATION_JSON));

        when(mockPriceRepository.findByProductId(anyString())).thenReturn(new Price("13860428", "12.34"));


        ProductSummary result = productService.getProductById("13860428");

        mockServer.verify();

        assertEquals("USD", result.getCurrencyCode());
        assertEquals("13860428", result.getId());
        assertEquals("The Big Lebowski (Blu-ray)", result.getName());
        assertEquals("12.34", result.getValue());
    }


    private String sampleResponse() {
        return "{\n" +
                "  \"product\": {\n" +
                "    \"deep_red_labels\": {\n" +
                "      \"total_count\": 2,\n" +
                "      \"labels\": [\n" +
                "        {\n" +
                "          \"count\": 0,\n" +
                "          \"id\": \"gqwm8i\",\n" +
                "          \"name\": \"TAC\",\n" +
                "          \"type\": \"relationship type\",\n" +
                "          \"priority\": 0\n" +
                "        },\n" +
                "        {\n" +
                "          \"count\": 0,\n" +
                "          \"id\": \"twbl94\",\n" +
                "          \"name\": \"Movies\",\n" +
                "          \"type\": \"merchandise type\",\n" +
                "          \"priority\": 0\n" +
                "        }\n" +
                "      ]\n" +
                "    },\n" +
                "    \"available_to_promise_network\": {\n" +
                "      \"product_id\": \"13860428\",\n" +
                "      \"id_type\": \"TCIN\",\n" +
                "      \"available_to_promise_quantity\": 38.0,\n" +
                "      \"street_date\": \"2011-11-15T06:00:00.000Z\",\n" +
                "      \"availability\": \"AVAILABLE\",\n" +
                "      \"online_available_to_promise_quantity\": 38.0,\n" +
                "      \"stores_available_to_promise_quantity\": 0.0,\n" +
                "      \"availability_status\": \"IN_STOCK\",\n" +
                "      \"multichannel_options\": [\n" +
                "        \"HOLD\"\n" +
                "      ]\n" +
                "    },\n" +
                "    \"item\": {\n" +
                "      \"tcin\": \"13860428\",\n" +
                "      \"bundle_components\": {\n" +
                "        \"is_assortment\": false,\n" +
                "        \"is_kit_master\": false\n" +
                "      },\n" +
                "      \"dpci\": \"058-34-0436\",\n" +
                "      \"upc\": \"025192110306\",\n" +
                "      \"product_description\": {\n" +
                "        \"title\": \"The Big Lebowski (Blu-ray)\",\n" +
                "        \"bullet_description\": [\n" +
                "          \"<B>Movie Genre:</B> Comedy\",\n" +
                "          \"<B>Software Format:</B> Blu-ray\",\n" +
                "          \"<B>Movie Studio:</B> Universal Studios\"\n" +
                "        ],\n" +
                "        \"general_description\": \"Blu-ray BIG LEBOWSKI, THE Movies\"\n" +
                "      },\n" +
                "      \"parent_items\": \"46767107\",\n" +
                "      \"buy_url\": \"http://www.target.com/p/the-big-lebowski-blu-ray/-/A-13860428\",\n" +
                "      \"enrichment\": {\n" +
                "        \"images\": [\n" +
                "          {\n" +
                "            \"base_url\": \"https://target.scene7.com/is/image/Target/\",\n" +
                "            \"primary\": \"13860428\"\n" +
                "          }\n" +
                "        ],\n" +
                "        \"sales_classification_nodes\": [\n" +
                "          {\n" +
                "            \"node_id\": \"55ayu\",\n" +
                "            \"wcs_id\": \"872533\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"node_id\": \"5xswx\",\n" +
                "            \"wcs_id\": \"4492\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"node_id\": \"5t0ak\",\n" +
                "            \"wcs_id\": \"41005\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      \"return_method\": \"This item can be returned to any Target store or Target.com.\",\n" +
                "      \"handling\": {\n" +
                "        \"import_designation_description\": \"Made in the USA or Imported\"\n" +
                "      },\n" +
                "      \"recall_compliance\": {\n" +
                "        \"is_product_recalled\": false\n" +
                "      },\n" +
                "      \"tax_category\": {\n" +
                "        \"tax_class\": \"G\",\n" +
                "        \"tax_code_id\": 99999,\n" +
                "        \"tax_code\": \"99999\"\n" +
                "      },\n" +
                "      \"display_option\": {\n" +
                "        \"is_size_chart\": false,\n" +
                "        \"is_warranty\": false\n" +
                "      },\n" +
                "      \"fulfillment\": {\n" +
                "        \"is_po_box_prohibited\": true,\n" +
                "        \"po_box_prohibited_message\": \"We regret that this item cannot be shipped to PO Boxes.\"\n" +
                "      },\n" +
                "      \"package_dimensions\": {\n" +
                "        \"weight\": \"0.18\",\n" +
                "        \"weight_unit_of_measure\": \"POUND\",\n" +
                "        \"width\": \"5.33\",\n" +
                "        \"depth\": \"6.65\",\n" +
                "        \"height\": \"0.46\",\n" +
                "        \"dimension_unit_of_measure\": \"INCH\"\n" +
                "      },\n" +
                "      \"environmental_segmentation\": {\n" +
                "        \"is_lead_disclosure\": false\n" +
                "      },\n" +
                "      \"manufacturer\": {\n" +
                "\n" +
                "      },\n" +
                "      \"product_classification\": {\n" +
                "        \"product_type\": \"542\",\n" +
                "        \"product_type_name\": \"ELECTRONICS\",\n" +
                "        \"item_type_name\": \"Movies\",\n" +
                "        \"item_type\": {\n" +
                "          \"category_type\": \"Item Type: MMBV\",\n" +
                "          \"type\": 300752,\n" +
                "          \"name\": \"Movies\"\n" +
                "        }\n" +
                "      },\n" +
                "      \"product_brand\": {\n" +
                "        \"brand\": \"Universal Home Video\"\n" +
                "      },\n" +
                "      \"item_state\": \"READY_FOR_LAUNCH\",\n" +
                "      \"specifications\": [\n" +
                "\n" +
                "      ],\n" +
                "      \"attributes\": {\n" +
                "        \"gift_wrapable\": \"N\",\n" +
                "        \"has_prop65\": \"N\",\n" +
                "        \"is_hazmat\": \"N\",\n" +
                "        \"max_order_qty\": 10,\n" +
                "        \"street_date\": \"2011-11-15\",\n" +
                "        \"media_format\": \"Blu-ray\",\n" +
                "        \"merch_class\": \"MOVIES\",\n" +
                "        \"merch_subclass\": 34,\n" +
                "        \"return_method\": \"This item can be returned to any Target store or Target.com.\"\n" +
                "      },\n" +
                "      \"country_of_origin\": \"US\",\n" +
                "      \"relationship_type_code\": \"Title Authority Child\",\n" +
                "      \"subscription_eligible\": false,\n" +
                "      \"ribbons\": [\n" +
                "\n" +
                "      ],\n" +
                "      \"tags\": [\n" +
                "\n" +
                "      ],\n" +
                "      \"estore_item_status_code\": \"A\",\n" +
                "      \"return_policies\": {\n" +
                "        \"user\": \"Regular Guest\",\n" +
                "        \"policyDays\": \"30\",\n" +
                "        \"guestMessage\": \"This item must be returned within 30 days of the ship date. See return policy for details.\"\n" +
                "      }\n" +
                "    }\n" +
                "  }\n" +
                "}";
    }
}
