-- lookup table of all per item product prices
drop table price;
CREATE TABLE price
(
  ID INTEGER GENERATED ALWAYS  AS IDENTITY,
  VALUE DECIMAL(6,2) NOT NULL

);

insert into student (value) values ('0.01');
insert into student (value) values ('1.00');
insert into student (value) values ('1.33');
insert into student (value) values ('1.99');
insert into student (value) values ('19.99');
-- and so on, or get clever like ARINC values
insert into student (value) values ('9999.99');
