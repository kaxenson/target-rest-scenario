package com.myretail.config;


import com.myretail.repositories.mongo.PriceRepository;
import com.myretail.service.ProductService;
import com.myretail.service.ProductServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan(basePackages = {"com.myretail.repositories", "com.myretail.controller"})
public class ApplicationConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean(name = "productService")
    public ProductService productService(PriceRepository priceRepository, RestTemplate restTemplate) {
        return new ProductServiceImpl(priceRepository, restTemplate);
    }
}
