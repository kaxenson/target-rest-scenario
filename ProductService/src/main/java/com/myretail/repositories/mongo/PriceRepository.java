package com.myretail.repositories.mongo;

import com.myretail.domain.mongo.Price;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PriceRepository extends MongoRepository<Price, String> {

    public Price findByProductId(String productId);
}