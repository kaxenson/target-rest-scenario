package com.myretail.domain.mongo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Document
public class Price implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull(message = "ProductId may not be null")
    @Pattern(regexp = "^\\d{8}", message = "ProductId is exactly 8 digits")
    private String productId;


    @Pattern(regexp = "^\\d{1,5}\\.\\d{2}")
    private String value;

    public Price(String productId, String value) {
        this.productId = productId;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getProductId() {
        return productId;
    }

    public String getValue() {
        return value;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id='" + id + '\'' +
                ", productId='" + productId + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
