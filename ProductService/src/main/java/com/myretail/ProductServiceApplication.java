package com.myretail;

import com.myretail.config.ApplicationConfig;
import com.myretail.controller.ProductApiController;
import com.myretail.domain.mongo.Price;
import com.myretail.repositories.mongo.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {ApplicationConfig.class})//, MongoConfig.class})
@EnableMongoRepositories(basePackageClasses = { PriceRepository.class})
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class ProductServiceApplication  {



	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProductServiceApplication.class);
	}

}
