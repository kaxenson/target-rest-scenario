package com.myretail.service;


import com.fasterxml.jackson.databind.JsonNode;
import com.myretail.domain.mongo.Price;
import com.myretail.model.Product;
import com.myretail.model.ProductSummary;
import com.myretail.repositories.mongo.PriceRepository;
import com.myretail.controller.ProductApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;


public class ProductServiceImpl implements ProductService {
    public static final Logger logger = LoggerFactory.getLogger(ProductApiController.class);

    private final PriceRepository priceRepository;

    private final RestTemplate restTemplate;

    public ProductServiceImpl(PriceRepository priceRepository, RestTemplate restTemplate) {
        this.priceRepository = priceRepository;
        this.restTemplate = restTemplate;
    }

    @Override
    public ProductSummary getProductById(String id) {
        Product product = getProductFromTargetRestService(id);
        logger.debug("getProductById: retrieved Product from REST service {}", product);
        if (product == null) {
            return null;
        }

        Price price = priceRepository.findByProductId(id);
        logger.debug("getProductById: retrieved Price from MongoDB {}", price);

        ProductSummary productSummary = new ProductSummary(id, product.getTitle(), price.getValue());
        logger.debug("getProductById: returning ProductSummary {}" + productSummary);

        return productSummary;
    }

    private Product getProductFromTargetRestService(String productId) {
        JsonNode rootNode = null;

        try {
            rootNode = restTemplate.getForObject("http://redsky.target.com/v2/pdp/tcin/" + productId + "?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics",
                    JsonNode.class);
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        if (rootNode == null) {
            return null;
        }
        String title = rootNode.path("product").path("item").path("product_description").path("title").asText();
        String fetchedProductId = rootNode.path("product").path("available_to_promise_network").path("product_id").asText();

        if (!productId.equals(fetchedProductId)) {
            logger.error("getProductFromTargetRestService: expected to receive the same product id expected={}, actual={}", productId, fetchedProductId);
        }

        return new Product(fetchedProductId, title);
    }

    @PostConstruct
    private void primePriceData() {
        // TODO: here for demo purposes

        if (priceRepository.count() != 0) {
            return;
        }

        // Prime Price data
        priceRepository.save(new Price("13860428", "14.99"));
        priceRepository.save(new Price("15117729", "1.99"));
        priceRepository.save(new Price("16483589", "19.99"));
        priceRepository.save(new Price("16696652", "0.99"));
        priceRepository.save(new Price("16752456", "1999.99"));
        priceRepository.save(new Price("15643793", "2.50"));

    }
}
