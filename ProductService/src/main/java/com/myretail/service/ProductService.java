package com.myretail.service;

import com.myretail.model.ProductSummary;


public interface ProductService {
    ProductSummary getProductById(String id);
}
