package com.myretail.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class ProductSummary {

    private String id;
    private String name;
    private String value;
    private final String currencyCode = "USD";

    public ProductSummary(String id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value =  value;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "ProductSummary{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
