package com.myretail.controller;

import com.myretail.model.Product;
import com.myretail.model.ProductSummary;
import com.myretail.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * A REST API endpoint for /product.
 */
@RestController
@RequestMapping("/product")
public class ProductApiController {
    public static final Logger logger = LoggerFactory.getLogger(ProductApiController.class);

    private final ProductService productService;

    @Autowired
    public ProductApiController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "/{id}")
    public ResponseEntity<ProductSummary> getProductById(@PathVariable String id) {
        ProductSummary productSummary = productService.getProductById(id);
        if (productSummary == null) {
            logger.error("getProductById: product id=[{}] not found", id);
            productSummary = new ProductSummary(id, "not found", "");
            new ResponseEntity<>(productSummary, HttpStatus.NOT_FOUND);
        }
        logger.info("getProductById: found {}", productSummary);
        return new ResponseEntity<>(productSummary, HttpStatus.OK);
    }

    @RequestMapping("/test")
    public ResponseEntity<String> test() {
        return new ResponseEntity<String>("Test", HttpStatus.OK);
}


}
